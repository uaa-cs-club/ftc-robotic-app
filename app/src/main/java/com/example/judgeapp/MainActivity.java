package com.example.judgeapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }
    public void openApp(String address){
        Uri uri = Uri.parse(address);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if(isIntentSafe)
            startActivity(intent);
    }

    public void ftcJudge(View view) {
        openApp("https://www.firstinspires.org/sites/default/files/uploads/resource_library/ftc/judge-and-judge-advisor-manual.pdf");
    }

    public void gameManual(View view) {
        openApp("https://www.firstinspires.org/sites/default/files/uploads/resource_library/ftc/game-manual-part-1.pdf");
    }

    public void thinkAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLSdA0NgK-Y6lnZN0pDSnYwrwsIO-d0hzWS_24i60OgUcaW55Lg/viewform");
    }
    public void inRoomJudging(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLScvqOTREmknWgMg1UuGf1X43mBbePNOB3sQNmgASLG356t5YA/viewform");
    }
    public void connectAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLSfkj0F4u4SxAdbjbLQBrU7EDKbfqCgrIczpw6kwKvFrmm2kiw/viewform");
    }
    public void innovateAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLScxtbpQQZDGIDeAJhz3UMEQFBNYAyEx_RCy0xV5uuj4M098Xw/viewform");
    }
    public void designAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLSdkTGpLFN6GdS5f_GWm9dk1-4PTx6z6g7HemVhGLrOuBiKD5Q/viewform");
    }
    public void motivateAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLSd8LNGWJiS_B-hj5tnkLIuZVdhB0qZjvEiiiNOTyyOJFY4vjw/viewform");
    }
    public void controlAward(View view){
        openApp("https://docs.google.com/forms/d/e/1FAIpQLSe0CsdY92ciTCOTW1WYe5DNu0RMJ1lJKA4rwrmM92HMcxT4xA/viewform");
    }

//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
