# FTC Robotic App

This is the current iteration of the robotic app with links to the Google forms.
This will DRASTICALLY change in the future! The physical program is in 
**app->java->com.example.judgeapp->MainActivity**
The basic design information is within **app->res**. The individual files 
correspond to layouts, colors, strings, etc.

More details about the app are in our shared google drive:
https://drive.google.com/drive/u/0/folders/1gYGgNy3qm_nH70mLDayjrjQpKdSURXfN

